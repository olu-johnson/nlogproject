﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace NLogProject.Logger
{
    public class WriteLogFile
    {
        public static bool WriteLog(string strFileName, Exception strMessage)
        {
            
            try
            {
                if (!Directory.Exists(AppDomain.CurrentDomain.BaseDirectory + "\\LogDetails"))
                {
                    Directory.CreateDirectory(AppDomain.CurrentDomain.BaseDirectory + "\\LogDetails");
                }


                FileStream objFilestream = new FileStream(string.Format("{0}\\{1}", AppDomain.CurrentDomain.BaseDirectory+"\\LogDetails", strFileName), FileMode.Append, FileAccess.Write);

                StringBuilder sb = new StringBuilder();
                sb.AppendLine();
                sb.Append("--------------------- Error Created on " + DateTime.Now + "------------------------");
                sb.AppendLine();
                sb.Append("Message: " + strMessage.Message);
                sb.AppendLine();
                sb.Append("Source: " + strMessage.Source);
                sb.AppendLine();
                sb.Append("StackTrace: " + strMessage.StackTrace);
                sb.AppendLine();
                sb.Append("Inner Exception: " + strMessage.InnerException);
                sb.AppendLine();
                sb.Append("------------------------------------------------------------------------------------");
                sb.AppendLine();
                StreamWriter objStreamWriter = new StreamWriter((Stream)objFilestream);
                objStreamWriter.WriteLine(sb);
                objStreamWriter.Close();
                objFilestream.Close();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}