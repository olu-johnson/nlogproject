﻿using NLog;
using NLogProject.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace NLogProject.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            try
            {
                int zero = 0;
                int c = 4 / zero;
            }
            catch (Exception ex)
            {
                
                //Logger logger = LogManager.GetCurrentClassLogger();
                //logger.ErrorException("Error", ex);

                WriteLogFile.WriteLog("ConsoleLog", ex);
            }
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}